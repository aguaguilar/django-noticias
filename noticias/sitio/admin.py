from django.contrib import admin

# Register your models here.

from .models import Categoria, Autor, Noticia

admin.site.register([Categoria,Autor,Noticia,])