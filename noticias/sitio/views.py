from django.shortcuts import render
from .models import Categoria, Noticia

# Create your views here.

def inicio(request):
    categorias = []
    categorias = Categoria.objects.all() #hace un select en la tabla categorias
    return render(request,"inicio.html",{"lista_categorias":categorias})

def detalle(request, categoria_id):
    noticias = []
    categoria = Categoria.objects.filter(id=categoria_id)
    noticias = Noticia.objects.filter(categoria=categoria)
    return render(request, "detalle.html", {"noticias":noticias})