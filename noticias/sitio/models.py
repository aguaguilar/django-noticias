from django.db import models

import _datetime
# Create your models here.


class Categoria(models.Model):
    nombre = models.CharField(max_length=50)

    def __str__(self):
        return self.nombre


class Autor(models.Model):
    nombre = models.CharField(max_length=50)
    apellido = models.CharField(max_length=50)
    fechaNacimiento = models.DateTimeField()

    def __str__(self):
        return self.nombre


class Noticia(models.Model):
    titulo = models.CharField(max_length=50)
    fecha = models.DateTimeField(default=_datetime.datetime.now())
    cuerpo = models.CharField(max_length=255)
    autor = models.ForeignKey(Autor)
    categoria = models.ForeignKey(Categoria)

    def __str__(self):
        return self.titulo
